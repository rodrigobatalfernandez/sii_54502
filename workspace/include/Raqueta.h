// Raqueta.h: interface for the Raqueta class.
// Rodrigo estuvo aquí.
//////////////////////////////////////////////////////////////////////

#pragma once
#include "Plano.h"
#include "Esfera.h"
#include "Vector2D.h"

class Raqueta : public Plano  
{
public:
	Vector2D velocidad;

	Raqueta();
	virtual ~Raqueta();

	void Mueve(float t);
};
