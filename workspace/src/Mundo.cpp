// Mundo.cpp: implementation of the CMundo class.
//
//////////////////////////////////////////////////////////////////////
#include <fstream>
#include "Mundo.h"
#include "glut.h"

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>

#include <unistd.h>
#include <fcntl.h>
#include <sys/mman.h>

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CMundo::CMundo()
{
	Init();
}

CMundo::~CMundo()
{

}

void CMundo::InitGL()
{
	//Habilitamos las luces, la renderizacion y el color de los materiales
	glEnable(GL_LIGHT0);
	glEnable(GL_LIGHTING);
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_COLOR_MATERIAL);
	
	glMatrixMode(GL_PROJECTION);
	gluPerspective( 40.0, 800/600.0f, 0.1, 150);
}

void print(char *mensaje, int x, int y, float r, float g, float b)
{
	glDisable (GL_LIGHTING);

	glMatrixMode(GL_TEXTURE);
	glPushMatrix();
	glLoadIdentity();

	glMatrixMode(GL_PROJECTION);
	glPushMatrix();
	glLoadIdentity();
	gluOrtho2D(0, glutGet(GLUT_WINDOW_WIDTH), 0, glutGet(GLUT_WINDOW_HEIGHT) );

	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();
	glLoadIdentity();

	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	glDisable(GL_DEPTH_TEST);
	glDisable(GL_BLEND);
	glColor3f(r,g,b);
	glRasterPos3f(x, glutGet(GLUT_WINDOW_HEIGHT)-18-y, 0);
	int len = strlen (mensaje );
	for (int i = 0; i < len; i++) 
		glutBitmapCharacter (GLUT_BITMAP_HELVETICA_18, mensaje[i] );
		
	glMatrixMode(GL_TEXTURE);
	glPopMatrix();

	glMatrixMode(GL_PROJECTION);
	glPopMatrix();

	glMatrixMode(GL_MODELVIEW);
	glPopMatrix();

	glEnable( GL_DEPTH_TEST );
}
void CMundo::OnDraw()
{
	//Borrado de la pantalla	
   	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	//Para definir el punto de vista
	glMatrixMode(GL_MODELVIEW);	
	glLoadIdentity();
	
	gluLookAt(0.0, 0, 17,  // posicion del ojo
		0.0, 0.0, 0.0,      // hacia que punto mira  (0,0,0) 
		0.0, 1.0, 0.0);      // definimos hacia arriba (eje Y)    

	/////////////////
	///////////
	//		AQUI EMPIEZA MI DIBUJO
	char cad[100];
	sprintf(cad,"Jugador1: %d",puntos1);
	print(cad,10,0,1,1,1);
	sprintf(cad,"Jugador2: %d",puntos2);
	print(cad,650,0,1,1,1);
	sprintf(cad,"Tiempo: %.2f",temporizador);
	print(cad,325,0,1,1,1);
	int i;
	for(i=0;i<paredes.size();i++)
		paredes[i].Dibuja();
	for(i=0;i<esferas.size();i++)
		esferas[i].Dibuja();

	fondo_izq.Dibuja();
	fondo_dcho.Dibuja();
	jugador1.Dibuja();
	jugador2.Dibuja();

	/////////////////
	///////////
	//		AQUI TERMINA MI DIBUJO
	////////////////////////////

	//Al final, cambiar el buffer
	glutSwapBuffers();
}

void CMundo::OnTimer(int value)
{
	int n;

	//Temporizador para la generación de esferas cada 10 segundos
	temporizador += 0.025f;
	if (temporizador > 10 ){
	temporizador = 0;
	Esfera e;
	esferas.push_back(e);
	}
	
	//Recepción de órdenes del bot
	switch(dir_compartida->accion){
		case 1:
		CMundo::OnKeyboardDown('o', 0, 0);
			break;
		case -1:
		CMundo::OnKeyboardDown('l', 0, 0);
			break;
		default:
			break;
	}
	
	//Movimientos
	jugador1.Mueve(0.025f);
	jugador2.Mueve(0.025f);
	for(int i = 0; i < esferas.size(); i++)
	{
		esferas[i].Mueve(0.0235f);
	}
	
	//Interacciones entre los elementos y las paredes
	for(int i=0;i<paredes.size();i++)
	{
		for(int j = 0; j < esferas.size(); j++)
		{
			paredes[i].Rebota(esferas[j]);
		}
		paredes[i].Rebota(jugador1);
		paredes[i].Rebota(jugador2);
	}
	
	
	//Rebotes entre esferas y jugadores
	for(int i = 0; i < esferas.size(); i++)
	{
		jugador1.Rebota(esferas[i]);
		jugador2.Rebota(esferas[i]);
	}
	
	
	//Rebotes entre esferas y fondos
	for(int i = 0; i < esferas.size(); i++)
	{
		if(fondo_izq.Rebota(esferas[i]))
		{
			esferas[i].centro.x=0;
			esferas[i].centro.y=rand()/(float)RAND_MAX;
			esferas[i].velocidad.x=2+2*rand()/(float)RAND_MAX;
			esferas[i].velocidad.y=2+2*rand()/(float)RAND_MAX;
			puntos2++;
			n = sprintf(buff, "Jugador 2 marca, puntos: %d\n", puntos2);
			write(fd1,buff,n);
		}

		if(fondo_dcho.Rebota(esferas[i]))
		{
			esferas[i].centro.x=0;
			esferas[i].centro.y=rand()/(float)RAND_MAX;
			esferas[i].velocidad.x=-2-2*rand()/(float)RAND_MAX;
			esferas[i].velocidad.y=-2-2*rand()/(float)RAND_MAX;
			puntos1++;
			n = sprintf(buff, "Jugador 1 marca, puntos: %d\n", puntos1);
			write(fd1,buff,n);
		}
	}
	
	//Actualización de la memoria compartida
	dir_compartida->esfera.centro.y = esferas[0].centro.y;
	dir_compartida->raqueta.y1 = jugador2.y1;
	dir_compartida->raqueta.y2 = jugador2.y2;
}

void CMundo::OnKeyboardDown(unsigned char key, int x, int y)
{
	switch(key)
	{
//	case 'a':jugador1.velocidad.x=-1;break;
//	case 'd':jugador1.velocidad.x=1;break;
	case 's':jugador1.velocidad.y=-4;break;
	case 'w':jugador1.velocidad.y=4;break;
	case 'l':jugador2.velocidad.y=-4;break;
	case 'o':jugador2.velocidad.y=4;break;
	case ' ':;break;	//Disparo de la raqueta

	}
}

void CMundo::Init()
{
//Recepción del nombre de la tubería
	int n;
	if((fd1 = open("mififo",O_WRONLY)) < 0)
		perror("Error de apertura FIFO en tenis\n");
	else
		printf("Apertura corecta de la FIFO; fd: %d\n", fd1);
		
//Generación del mapeo de memoria
	if((fd2 = open("memcompartida.txt",O_RDWR|O_CREAT, 0666)) < 0)
		perror("Error de apertura del fichero de memoria compartida\n");
	ftruncate(fd2, sizeof(dir_compartida));
	dir_compartida = (datosMemCompartida*)mmap(NULL, sizeof(datosMemCompartida), PROT_READ|PROT_WRITE, MAP_SHARED, fd2, 0);
	close(fd2);
	
//Temporizador puesto a 0
	temporizador = 0;
	
//pared inferior
	Plano p;
	p.x1=-7;p.y1=-5;
	p.x2=7;p.y2=-5;
	paredes.push_back(p);

//superior
	p.x1=-7;p.y1=5;
	p.x2=7;p.y2=5;
	paredes.push_back(p);

	fondo_izq.r=0;
	fondo_izq.x1=-7;fondo_izq.y1=-5;
	fondo_izq.x2=-7;fondo_izq.y2=5;

	fondo_dcho.r=0;
	fondo_dcho.x1=7;fondo_dcho.y1=-5;
	fondo_dcho.x2=7;fondo_dcho.y2=5;

//Jugador
	//a la izq
	jugador1.g=0;
	jugador1.x1=-6;jugador1.y1=-1;
	jugador1.x2=-6;jugador1.y2=1;

	//a la dcha
	jugador2.g=0;
	jugador2.x1=6;jugador2.y1=-1;
	jugador2.x2=6;jugador2.y2=1;
	
//Primera esfera
	Esfera e;
	esferas.push_back(e);
}
