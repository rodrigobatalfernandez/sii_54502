#include <unistd.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <sys/mman.h>
#include <stdio.h>
#include "glut.h"

#include "datosMemCompartida.h"
#include "Esfera.h"
#include "Raqueta.h"

int main(){
	int fd;;
	float y_esfera, y_raqueta;
	datosMemCompartida* dir_compartida;
	
	if((fd = open("memcompartida.txt",O_RDWR)) < 0)
		perror("Error de apertura del fichero de memoria compartida\n");
	dir_compartida = (datosMemCompartida*)mmap(NULL, sizeof(dir_compartida), PROT_READ | PROT_WRITE, MAP_SHARED, fd, 0);
	close(fd);
	while(true){
	y_esfera = dir_compartida->esfera.centro.y;
	y_raqueta = 0.5*(dir_compartida->raqueta.y1 + dir_compartida->raqueta.y2);
	
	if(y_esfera > y_raqueta)
		dir_compartida->accion = 1;
	else if(y_esfera < y_raqueta)
		dir_compartida->accion = -1;
	else
		dir_compartida->accion = 0;
	usleep(25000);
	}
	return 0;
}
