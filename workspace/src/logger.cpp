#include <unistd.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <stdio.h>
#include <string.h>

int main(int argc, char* argv[]){
	int fd, n;
	char buff[4096];
	
	printf("Logger Iniciado\n");
	
	if(argc != 2){
		perror("Nº de argumentos inválido");
		return -1;
		}
	if(argv[1][0] == 'a'){		//Abrir
		printf("Intentando abrir\n");
		if((fd = open("mififo",O_RDONLY)) < 0){	//Pero por qué fd=0 ?!?!?!?!?
			perror("Error de apertura del archivo");
			return -1;
		}
		printf("Logger Abierto; fd:%d\n",fd);
		while(true){
			n=read(fd,buff,sizeof(buff));
			printf("%s", buff);
		}
		close(fd);
		printf("Logger Cerrado");
	}
	else if(argv[1][0] == 'c'){	//Crear
		if (mkfifo("mififo", 0666) < 0){
			perror("Error de creación del archivo");
			return -1;
			}
		printf("Logger Creado\n");
	}
	else if(argv[1][0] == 'd'){	//Destruir
		printf("Logger Destruido\n");
		unlink("mififo");
	}
	else
		printf("Argumento no reconocido\n");
	
	return 0;
}
