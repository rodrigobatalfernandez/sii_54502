# Changelog
Todos los cambios relevantes serán documentados a continuación.

[v1.0] - Versión Inicial

[v1.1] - Primera versión propia

	- Añadido
	Este mismo Changelog para mantener un registro de los cambios
	Marcas en las fuentes para denotar propiedad
	
	- Cambiado
	Nada
	
[v1.2] - Final de la Práctica 1

	- Añadido
	Nada
	
	- Cambiado
	Método Mueve() de la clase Esfera, ahora en efecto, se mueve
	atributos de la clase Mundo, incluido un vector dinámico de esferas
	Métodos de la clase Mundo, trabajo con vector de esferas (múltiples esferas)
	
[v1.3] - Final de la Práctica 2

	- Añadido
	Nuevos ejecutables logger y bot con las funcionalidades designadas en la práctica
	Nueva clase datosMemCompartida
	Funcionalidad en Mundo para interactuar con fifos y ficheros, y mapeos en memoria.
	
	- Cambiado
	Atributos de la clase Mundo para albergar descriptores de ficheros y una instancia de datosMemCompartida
	Aportado un "pragma once" a Tenis.h que era MUY NECESARIO
